package org.to.context;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.to.marshaller.DefaultMarshaller;
import org.to.marshaller.Marshaller;

public class XmlContext {

    private static Logger log = LogManager.getLogger(XmlContext.class);

    private final Marshaller marshaller;

    private XmlContext(Class<?> type) {
        this.marshaller = new DefaultMarshaller(type);
    }

    public static XmlContext create(Class<?> type) {
        return new XmlContext(type);
    }

    public Marshaller getMarshaller() {
        return this.marshaller;
    }
}
