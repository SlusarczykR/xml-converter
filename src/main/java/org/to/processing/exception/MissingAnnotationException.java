package org.to.processing.exception;

public class MissingAnnotationException extends Exception {

    public MissingAnnotationException() {
    }

    public MissingAnnotationException(String message) {
        super(message);
    }
}
