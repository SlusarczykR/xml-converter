package org.to.validator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.to.processing.annotation.XmlRootElement;

import java.util.Optional;

public class XmlTypeValidator {

    private static Logger log = LogManager.getLogger(XmlTypeValidator.class);

    private static XmlTypeValidator instance;

    private XmlTypeValidator() {
    }

    public static XmlTypeValidator getInstance() {
        if (instance == null) {
            instance = new XmlTypeValidator();
        }
        return instance;
    }

    public <T> boolean validate(T object, Class<?> type) {
        if (object.getClass().equals(type) && hasTypeRootElementName(type)) {
            return true;
        }
        return false;
    }

    private boolean hasTypeRootElementName(Class<?> type) {
        return Optional.ofNullable(type.getDeclaredAnnotation(XmlRootElement.class)).isPresent();
    }

    private boolean hasAtLeastOneElementName(Class<?> type) {
        return Optional.ofNullable(type.getDeclaredAnnotation(XmlRootElement.class)).isPresent();
    }
}
