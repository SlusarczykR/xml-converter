package org.to.marshaller;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.to.processing.annotation.XmlElement;
import org.to.processing.annotation.XmlRootElement;
import org.to.processing.exception.MissingAnnotationException;
import org.to.validator.XmlTypeValidator;

import java.io.File;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

import static org.to.processing.contant.XmlElementConstant.DEFAULT_NAME;

public class DefaultMarshaller implements Marshaller {

    private static Logger log = LogManager.getLogger(DefaultMarshaller.class);

    private final Class<?> type;
    private XmlTypeValidator typeValidator;

    public DefaultMarshaller(Class<?> type) {
        this.type = type;
        this.typeValidator = XmlTypeValidator.getInstance();
    }

    @Override
    public <T> void marshal(T object, File output) {
        if (typeValidator.validate(object, type)) {
            String result = marshal(object);
        }
    }

    private <T> String marshal(T object) {
        Set<String> xmlElementsNames = getXmlElementsNames();

        return null;
    }

    public String getRootElementName(Class<?> type) throws MissingAnnotationException {
        if (hasRootElementAnnotation(type)) {
            return getTypeRootElementName(type);
        }
        throw new MissingAnnotationException("");
    }

    private boolean hasRootElementAnnotation(Class<?> entityType) {
        return Arrays.stream(entityType.getDeclaredAnnotations())
                .anyMatch(annotation -> annotation instanceof XmlRootElement);
    }

    private String getTypeRootElementName(Class<?> type) {
        String rootElementName = type.getDeclaredAnnotation(XmlRootElement.class).name();

        if (isDefaultName(rootElementName)) {
            return type.getSimpleName();
        }
        return rootElementName;
    }

    private boolean isDefaultName(String name) {
        return StringUtils.isBlank(name) || name.equals(DEFAULT_NAME);
    }

    public Set<String> getXmlElementsNames() {
        return getXmlElements().stream()
                .map(Field::getName)
                .collect(Collectors.toSet());
    }

    public Set<Field> getXmlElements() {
        return Arrays.stream(type.getDeclaredFields())
                .filter(this::isXmlElement)
                .collect(Collectors.toSet());
    }

    private boolean isXmlElement(Field field) {
        return Arrays.stream(field.getDeclaredAnnotations())
                .anyMatch(annotation -> annotation instanceof XmlElement);
    }
}
