package org.to.marshaller;

import java.io.File;

public interface Marshaller {
    <T> void marshal(T source, File output);
}
